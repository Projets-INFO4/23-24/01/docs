# Dashboard

-------

# January 29<sup>th

## Problematics :
What's the project about ? 
What's a pentest application ?

##  Task of the day :
- All : creation of the Trello
- All : deep understanding of the project

-------

# February 12<sup>th

## Problematics :
What's Docker ? How to use it ?

## Task of the day :
- All : started to learn about Docker
- All : installed Docker 
    - Clément : installed Docker Desktop (Windows)
    - Bastien - Anastasios - Alexis : installed using terminal (Linux)
-------

# February 19<sup>th

## Problematics :
Wich Pentest tools should we use ?

## Task of the day :
- All : searched about pentest tools
- Clément : implemented a VM of Kali linux on his computer
- Clément : started to use Nikto
- Bastien : searched about technologies for the API
- Alexis - Anastasios : continued to learn about Docker

## Decisions : 
Use of Nikto. Use of Exegol ?
Use of Actix. 

-------

# March 4<sup>th

## Problematics :
How to structure the project ?

## Decisions : 
Decided to have one container per tool. 
We must start the container for each test we want to run.

-------

# March 11<sup>th

## Problematics :
How do sequence the launch of containers ?
How to launch a specific tool with the right arguments
Use of Docker compose ?
How to share the reports between the containers ?

## Task of the day :
- Clément : started to develop Dockerfiles to test creating images
- Bastien - Anastasios : started to develop the API
- Alexis : learned about Docker volumes docker volumes

-------

# March 14<sup>th

## Task of the day :
- Clément - Anastasios - Alexis : started to create containers and Dockerfiles for each tools
- Bastien : continued to develop the API

## Decisions : 
Tools we will use : Nikto, Dirbuster, Whatweb, Nmap

-------

# March 15<sup>th

## Task of the day :
- Anastasios : added Hydra and smb tool
- Bastien : continued to develop the API
- Clément - Alexis : learned how to use the diffferent tools and created documentation

## Decisions : 
Added Hydra and smb

-------

# March 18<sup>th

## Task of the day :
- Clément : added searchsploit tool
- Clément : tested DockerCompose
- Bastien : developed the API
- Bastien : discovered the way to use the Docker API 
- Anastasios - Alexis : runed tests with the Docker Compose

-------

# March 19<sup>th

## Task of the day :
- Bastien - Alexis : tried the API with the Docker Compose
- Clément - Anastasios : added sqlmap and xsstrike tool

-------

# March 22<sup>th

## Task of the day :
- Clément : debugged API
- Bastien - Anastasios : added handlers for docker
- Clément - Alexis : managed report storage

-------

# March 25<sup>th

## Task of the day :
- Clément : modified start scripts for different tools
- Alexis : tried to create an nmap container
- Bastien - Anastasios : worked on the API

-------

# March 26<sup>th

## Task of the day :
- Clément - Anastasios : developed the frontend
- Bastien : added Docker API endpoint, volume management, parameters management to the API
- Alexis : fixed some scripts

-------

# March 29<sup>th

## Task of the day :
- Clément - Alexis : changed scripts => no more directory or date in parameters
- Bastien - Anastasios : resolved issues with the API

-------

# April 2<sup>nd

## Task of the day :
- Bastien - Anastasios : merged the API and the frontend
- Clément - Alexis : tried resolving problems on searchploit tool

-------

# April 5<sup>th

## Task of the day :
- All : created a version with all tools
- Clément - Anastasios : fixed last bugs
- Alexis : created a Makefile to build automatically images

-------

# April 8<sup>th

## Task of the day :
- All : worked on the presentation
