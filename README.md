# Projet pentest : 

Ce projet permet d'obtenir des comptes-rendus de pentests aux formats json/csv concernant des sites web.

## Fonctionnalités :
- Scan de vulnérabilités (XSS, SQL, formulaire)
- Scan de ports du serveur web
- Scan des différentes pages accessibles depuis le client
- Liste l'ensemble des technologies du site
- Recherche de CVE lié au version des technologies utilisés




### Architecture : 

![architexture](./Projet_PENTEST.svg)

### Docker Engine API

Il est nécessaire d'activer l'API de Docker engine.
Par défaut, une socket unix domain est ouverte à `/var/run/docker.sock``. C'est une socket de communication inter-processus (inter-process communication, IPC, en anglais).
Il est possible d'utiliser des requêtes UDS via l'orchestrateur puisque le framework Actix prend en compte ce format de requête.

**Pour nous simplifier la tâche au début, nous avons décider d'aussi ouvrir une socket TCP.**

On peut ajouter une socket TCP par défaut en ajoutant dans le fichier :  `/etc/systemd/system/docker.service.d/override.conf` les lignes : 

```docker
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:2375
```

<span style="color:red">**Attention, l'API est accessible sans securité ! Pour sécuriser cette connexion, on peut ajouter un certificat auto signée.**

Il faut configurer le démon docker pour activer les connections TLS en utilisant notre certificat. Il faut placer tous les fichiers nécessaire à la verification dans  `/etc/ssl` et mettre à jour `/etc/systemd/system/docker.service.d/override.conf` avec : 

```docker
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:2376 --tlsverify --tlscacert=/etc/ssl/certs/ca.pem --tlscert=/etc/ssl/certs/server-cert.pem --tlskey=/etc/ssl/private/server-key.pem
``` 
